rm(list = ls())
TIMESTAMPSALARM = NULL
TIMESTAMPSALARM2 = NULL
TIMESTAMPSALARM3 = NULL
TIMESTAMPSALARM4 = NULL
METERCALLED = 0
ltcutoff = .001
CABLOSSTOPRINT = 0
INSTCAP=c(500.5,500.5)
FILEPATHCOMP = ""
Eac1Recorded = 0
Eac2Recorded = 0
GModRecorded = 0
Yld1Recorded = 0
Yld2Recorded = 0
EacNew1 = EacNew2 = 0

resetGlobals = function()
{
Eac1Recorded <<- 0
Eac2Recorded <<- 0
EacNew1 <<- 0
EacNew2 <<- 0
GModRecorded <<- 0
Yld1Recorded <<- 0
Yld2Recorded <<- 0
}

timetomins = function(x)
{
  hr = unlist(strsplit(x,":"))
	seq1 = seq(from = 1,to = length(hr),by =2)
	seq2 = seq(from = 2,to = length(hr),by =2)
  min = as.numeric(hr[seq2]) 
  hr = as.numeric(hr[seq1]) * 60
  return((hr + min + 1))
}
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}
getPRData = function(df,no)
{
	PR1=PR2=gsi=NA
	date = as.character(df[,1])
	yr = substr(date,1,4)
	mon=substr(date,1,7)
	date = substr(date,1,10)
	path = paste('/home/admin/Dropbox/Second Gen/[KH-714S]/',yr,"/",mon,"/[KH-714S] ",date,".txt",sep="")
	print(paste('Path reading GSI is from',path))
	if(file.exists(path))
	{
	print('File exists')
	dataread = read.table(path,header = T,sep = "\t",stringsAsFactors=F)
	{
	if(no==2 || no==4){
	dsp1 = as.numeric(df[1,9])
	dsp2 = as.numeric(df[1,10])
	}
	else if(no==3 || no==1)
		dsp1 = as.numeric(df[1,5])
	}
	gsi = as.numeric(dataread[1,6])
	GModRecorded <<- gsi
	PR1 = format(round((dsp1 *100/ gsi),1),nsmall=1)
	if(no ==2 || no == 4)
		PR2 = format(round((dsp2 *100/ gsi),1),nsmall=1)
	}
	array2 = c(PR1,gsi)
	if(no == 2 || no ==4)
		array2 = c(PR1,PR2,gsi)
	return(array2)
}
patchSolar = function(path, ghi)
{
	pathMSB1 = gsub("Sensors","MSB-01-Solar",path)
	pathMSB1 = gsub("M5","M2",pathMSB1)
	pathMSB2 = gsub("Sensors","MSB-02-Solar",path)
	pathMSB2 = gsub("M5","M4",pathMSB2)
	pathComAp1 = gsub("Sensors","ComAp-01",path)
	pathComAp1 = gsub("M5","M1",pathComAp1)
	pathComAp2 = gsub("Sensors","ComAp-02",path)
	pathComAp2 = gsub("M5","M3",pathComAp2)
	print("------------------------")
	print(pathMSB1)
	print(pathMSB2)
	print("------------------------")
	if(file.exists(pathMSB1))
	{
		dataread = read.table(pathMSB1,header = T,sep ="\t",stringsAsFactors=F)
		if(ncol(dataread) < 10)
		{
			print(paste('PathMSB1',pathMSB1,'has lesser than 10 cols so returning'))
			return()
		}
		pr1 = round(as.numeric(dataread[,9])*100/ghi,1)
		pr2 = round(as.numeric(dataread[,10])*100/ghi,1)
		pr1og = as.numeric(dataread[,11])
		pr2og = as.numeric(dataread[,12])
		dataread[,13]=ghi
		dataread[,11]=pr1
		dataread[,12]=pr2
    write.table(dataread,file=pathMSB1,sep="\t",append=F,col.names =T,row.names=F)
		print(paste("Data patched from",pr1og,"to",pr1))
		print(paste("Data patched from",pr2og,"to",pr2))
	}
	if(file.exists(pathMSB2))
	{
		dataread = read.table(pathMSB2,header = T,sep ="\t",stringsAsFactors=F)
		if(ncol(dataread) < 10)
		{
			print(paste('PathMSB1',pathMSB2,'has lesser than 10 cols so returning'))
			return()
		}
		pr1 = round(as.numeric(dataread[,9])*100/ghi,1)
		pr2 = round(as.numeric(dataread[,10])*100/ghi,1)
		pr1og = as.numeric(dataread[,11])
		pr2og = as.numeric(dataread[,12])
		dataread[,13]=ghi
		dataread[,11]=pr1
		dataread[,12]=pr2
		write.table(dataread,file=pathMSB2,sep="\t",append=F,col.names =T,row.names=F)
		print(paste("Data patched from",pr1og,"to",pr1))
		print(paste("Data patched from",pr2og,"to",pr2))
	}
	if(file.exists(pathComAp1))
	{
		dataread = read.table(pathComAp1,header = T,sep ="\t",stringsAsFactors=F)
		if(ncol(dataread) < 7)
		{
			print(paste('Pathcomap1',pathComAp1,'has lesser than 7 cols so returning'))
			return()
		}
		pr1 = round(as.numeric(dataread[,5])*100/ghi,1)
		pr1og = as.numeric(dataread[,6])
		dataread[,7]=ghi
		dataread[,6]=pr1
		write.table(dataread,file=pathComAp1,sep="\t",append=F,col.names =T,row.names=F)
		print(paste("Data patched from",pr1og,"to",pr1))
	}
	if(file.exists(pathComAp2))
	{
		dataread = read.table(pathComAp1,header = T,sep ="\t",stringsAsFactors=F)
		if(ncol(dataread) < 7)
		{
			print(paste('Pathcomap2',pathComAp1,'has lesser than 7 cols so returning'))
			return()
		}
		pr1 = round(as.numeric(dataread[,5])*100/ghi,1)
		pr1og = as.numeric(dataread[,6])
		dataread[,7]=ghi
		dataread[,6]=pr1
		write.table(dataread,file=pathComAp2,sep="\t",append=F,col.names =T,row.names=F)
		print(paste("Data patched from",pr1og,"to",pr1))
	}
}
if(F){
getArtificialLoad = function(filepath)
{
	print('Artificial load')
	print('--------------------------------')
	print(filepath)
	print(FILEPATHCOMP)
	print('--------------------------------')
	dataread = try(read.table(filepath,header=T,sep = "\t",stringsAsFactors=F),silent = T)
	dataread2 = try(read.table(FILEPATHCOMP,header=T,sep="\t",stringsAsFactors=F),silent = T)
	if(class(dataread)=='try-error' || class(dataread2)=='try-error' || ncol(dataread2) < 20)
	{
		return (NA)
	}
	artificialLoad = unlist(rep(NA,length(dataread[,1])))
	c1 = as.numeric(dataread[,15])
	c2 = as.numeric(dataread2[,20])
	idxmtch = match(as.character(dataread2[,1]),as.character(dataread[,1]))
	idxmtch2 = match(as.character(dataread[,1]),as.character(dataread2[,1]))
	idxmtch = idxmtch[complete.cases(idxmtch)]
	idxmtch2 = idxmtch2[complete.cases(idxmtch2)]
	c1 = c1[idxmtch]
	c2 = c2[idxmtch2]
	artificialLoad[idxmtch] = c1 + c2
	cnms = colnames(dataread)
	dataread[,(ncol(dataread)+1)] = as.character(artificialLoad)
	colnames(dataread) = c(cnms,"ArtificialLoad")
	print('Updating Gen-1 data')
	write.table(dataread,file=filepath,row.names=F,col.names=T,sep="\t",append=F)
	return(round(sum(artificialLoad[complete.cases(artificialLoad)])/60000,2))
}
}

getArtificialLoad = function(no)
{
	if(no == 1)
		return((EacNew1 +  Eac1Recorded))
	if(no == 2)
		return((EacNew2 + Eac2Recorded))
}

secondGenData = function(filepath,writefilepath)
{
  {
		if(METERCALLED == 1){
	 		TIMESTAMPSALARM <<- NULL
			TIMESTAMPSALARM2 <<- NULL
			TIMESTAMPALARM3 <<- NULL
			TIMESTAMPALARM4 <<- NULL}
		else if(METERCALLED == 2){
			TIMESTAMPSALARM2 <<- NULL}
		else if(METERCALLED == 3)
		{
			TIMESTAMPALARM3 <<- NULL
		}
		else if(METERCALLED ==4)
		{
			TIMESTAMPALARM4 <<- NULL
		}
	}
	if(METERCALLED == 5)
	{
  	dataread = try(read.table(filepath,header = T,sep = "\t",stringsAsFactors=F),silent=T)
		if(class(dataread)=='try-error')
		{
			df = data.frame(Tm=NA,GHI=NA,Tamb=NA,TambMx=NA,TambMin=NA,DA=NA,stringsAsFactors=F)
			write.table(df,file = writefilepath,row.names = F,col.names = T,sep="\t",append = F)
  		return(df)
		}
		date = substr(as.character(dataread[1,1]),1,10)
		DA = round(nrow(dataread)/14.4,1)
		TAMBMX = TAMBMN = TAMB = GHI = NA
		ghi = as.numeric(dataread[,2])
		ghi = ghi[complete.cases(ghi)]
		if(length(ghi))
		{
			GHI = round(sum(ghi)/60000,2)
		}
		tamb = as.numeric(dataread[,3])
		tamb = tamb[complete.cases(tamb)]
		if(length(tamb))
		{
			TAMB = round(mean(tamb),1)
			TAMBMN = round(min(tamb),1)
			TAMBMX = round(max(tamb,1))
		}
		df = data.frame(Tm=date,Ghi=GHI,Tamb=TAMB,TambMx=TAMBMX,TambMn=TAMBMN,DA=DA, stringsAsFactors=F)
		write.table(df,file = writefilepath,row.names = F,col.names = T,sep="\t",append = F)
		patchSolar(writefilepath,GHI)
  	return(df)
	}
	print(paste('IN 2G',filepath))
  dataread = try(read.table(filepath,header = T,sep = "\t",stringsAsFactors=F),silent=T)
	if(class(dataread)=='try-error' || ncol(dataread) < 15)
	{
		{
		if(METERCALLED == 1 || METERCALLED == 3)
		{
      df = data.frame(Date = NA, LoadEac = NA,
                  DA = NA,Downtime = NA,Yld=NA, PR = NA, GSITot = NA,stringsAsFactors = F)
		}
		else if (METERCALLED == 2 || METERCALLED == 4)
		{
      df = data.frame(Date = NA, SolarPowerMeth1 = NA,SolarPowerMeth2= NA,
                  Ratio=NA,DA = NA,Downtime = NA,
									LastTime = NA, LastRead = NA, Yld1=NA,Yld2=NA,
									PR1 =NA, PR2 =NA,GSITot=NA,ArtificialLoad=NA,stringsAsFactors = F)
		}
		}
		write.table(df,file = writefilepath,row.names = F,col.names = T,sep="\t",append = F)
  	return(df)
	}
	dataread2 = dataread
	if(METERCALLED == 2 || METERCALLED == 4)
	{
	dataread = dataread2[complete.cases(as.numeric(dataread2[,46])),]
	lasttime=lastread=Eac2=NA
	if(nrow(dataread)>0)
	{
	lasttime = as.character(dataread[nrow(dataread),1])
	lastread = as.character(round(as.numeric(dataread[nrow(dataread),46])/1000,3))
	print(lastread)
  Eac2 = format(round((as.numeric(dataread[nrow(dataread),46]) - as.numeric(dataread[1,46]))/1000,2),nsmall=1)
	if(METERCALLED == 2)
		Eac1Recorded <<- as.numeric(Eac2)
	if(METERCALLED == 4)
		Eac2Recorded <<- as.numeric(Eac2)
	}
	}
	dataread = dataread2[complete.cases(as.numeric(dataread2[,19])),]
	dataread = dataread[as.numeric(dataread[,19]) > 0,]
	Eac1 = RATIO=NA
	if(nrow(dataread) > 1)
	{
   	Eac1 = format(round(sum(as.numeric(dataread[,19]))/60000,1),nsmall=2)
		if(METERCALLED == 2 || METERCALLED == 4)
			RATIO = round(as.numeric(Eac1)*100/as.numeric(Eac2),1)
	}
	datareadOrig = dataread2
	dataread = dataread2
  DA = format(round(nrow(dataread)/14.4,1),nsmall=1)
  tdx = timetomins(substr(dataread[,1],12,16))
  dataread2 = dataread[tdx > 480,]
  tdx = tdx[tdx > 480]
  dataread2 = dataread2[tdx < 1020,]
	dataread2 = dataread2[complete.cases(as.numeric(dataread2[,19])),]
  missingfactor = 540 - nrow(dataread2)
  dataread2 = dataread2[as.numeric(dataread2[,19]) < ltcutoff,]
	if(length(dataread2[,1]) > 0)
	{
		{
			if(METERCALLED == 1){
				TIMESTAMPSALARM <<- as.character(dataread2[,1])}
			else if(METERCALLED == 2){
		 	 TIMESTAMPSALARM2 <<- as.character(dataread2[,1])}
			else if(METERCALLED == 3){
		 	 TIMESTAMPSALARM3 <<- as.character(dataread2[,1])}
			else if(METERCALLED == 4){
		 	 TIMESTAMPSALARM4 <<- as.character(dataread2[,1])}
		}
	}
  totrowsmissing = format(round((missingfactor + nrow(dataread2))/5.4,1),nsmall=1)
	date = NA
	if(nrow(dataread)>0)
		date = substr(as.character(dataread[1,1]),1,10)
		indextouse = ceiling(METERCALLED/2)
	{
	  if(METERCALLED == 1 || METERCALLED == 3)
	  {
			dataread2 = datareadOrig
			dataread = dataread2[complete.cases(as.numeric(dataread2[,20])),]
			dataread = dataread[as.numeric(dataread[,20]) > 0,]
			if(nrow(dataread) > 1)
			{
   			Eac1 = format(round(sum(as.numeric(dataread[,20]))/60000,1),nsmall=2)
			}
			Yld = format(round(as.numeric(Eac1)/INSTCAP[indextouse],2),nsmall=2)
      df = data.frame(Date = date, LoadEac = as.numeric(Eac1),
                  DA = DA,Downtime = totrowsmissing,Yld=Yld,stringsAsFactors = F)
			vals = getPRData(df,METERCALLED)
			if(METERCALLED == 1)
				EacNew1 <<- as.numeric(Eac1)
			if(METERCALLED == 3)
				EacNew2 <<- as.numeric(Eac1)

      df = data.frame(Date = date, LoadEac = as.numeric(Eac1),
                  DA = DA,Downtime = totrowsmissing,Yld=Yld, PR = vals[1], GSITot = vals[2],stringsAsFactors = F)
    } 
	  else if(METERCALLED == 2 || METERCALLED == 4)
	  {
			Yld1 = format(round(as.numeric(Eac1)/INSTCAP[indextouse],2),nsmall=2)
			Yld2 = format(round(as.numeric(Eac2)/INSTCAP[indextouse],2),nsmall=2)
			if(METERCALLED == 2)
				Yld1Recorded <<- as.numeric(Yld2)
			if(METERCALLED == 4)
				Yld2Recorded <<- as.numeric(Yld2)
			df = data.frame(Date = date, SolarPowerMeth1 = as.numeric(Eac1),SolarPowerMeth2= as.numeric(Eac2),
                  Ratio=RATIO,DA = DA,Downtime = totrowsmissing,
									LastTime = lasttime, LastRead = lastread, Yld1=Yld1,Yld2=Yld2,stringsAsFactors = F)
			vals = getPRData(df,METERCALLED)
      artificial = getArtificialLoad(METERCALLED/2)
      
      npath = gsub('MSB-01-Solar','Sensors',filepath)	
      npath = gsub("M2","M5",npath)	
      GA=PA=0	
      if(file.exists(npath)){	
        ndf = read.table(npath, header = T, sep ="\t", stringsAsFactors=F)	
        timestamps_irradiance_greater_20 = ndf[complete.cases(ndf[, 2]>20), 1]	
        timestamps_freq_greater_40 = dataread[complete.cases(dataread[,2]>40), 1]	
        timestamps_pow_greater_2 = dataread[complete.cases(dataread[,19]>2), 1]	
        common = intersect(timestamps_irradiance_greater_20, timestamps_freq_greater_40)	
        common2 = intersect(common, timestamps_pow_greater_2)	
        GA = round(((length(common)/length(timestamps_irradiance_greater_20))*100), 1)	
        PA = round(((length(common2)/length(common))*100), 1)	
      }
      
      df = data.frame(Date = date, SolarPowerMeth1 = as.numeric(Eac1),SolarPowerMeth2= as.numeric(Eac2),
                  Ratio=RATIO,DA = DA,Downtime = totrowsmissing,
									LastTime = lasttime, LastRead = lastread, Yld1=Yld1,Yld2=Yld2,
									PR1 = vals[1], PR2 = vals[2],GSITot=vals[3],ArtificialLoad=artificial,GA=GA,PA=PA,stringsAsFactors = F)
		}
	}
	write.table(df,file = writefilepath,row.names = F,col.names = T,sep="\t",append = F)
  return(df)
}

thirdGenData = function(filepathm1, filepathm2,filepathm3,filepathm4,filepathm5,writefilepath)
{
  dataread1 =read.table(filepathm1,header = T,sep="\t",stringsAsFactors=F) #its correct dont change
  dataread2 = read.table(filepathm2,header = T,sep = "\t",stringsAsFactors=F) #its correct dont change
	dataread3 = read.table(filepathm3,header = T,sep = "\t",stringsAsFactors=F) #its correct dont change
	dataread4 = read.table(filepathm4,header = T,sep = "\t",stringsAsFactors=F) #its correct dont change
	artificial1 = as.numeric(dataread2[,13])
	artificial2 = as.numeric(dataread4[,13])

	dt = as.character(dataread2[,6])
	da = as.character(dataread2[,5])
	EacLoad1 = as.numeric(dataread1[,2])
	EacLoad2 = as.numeric(dataread3[,2])
	EacSol1Meth1 = as.numeric(dataread2[,2])
	EacSol1Meth2 = as.numeric(dataread2[,3])
	RatioSol1 = as.numeric(dataread2[,4])
	EacSol2Meth1 = as.numeric(dataread4[,2])
	EacSol2Meth2 = as.numeric(dataread4[,3])
	RatioSol2 = as.numeric(dataread4[,4])
	LastReadSol1 = as.numeric(dataread2[,8])
	LastReadSol2 = as.numeric(dataread4[,8])
	LastTimeSol1 = as.numeric(dataread2[,7])
	LastTimeSol2 = as.numeric(dataread4[,7])
	GsiTot = as.numeric(dataread1[,7])
	PR11 = as.numeric(dataread1[,6])
	PR31 = as.numeric(dataread3[,6])
	PR21 = as.numeric(dataread2[,11])
	PR22 = as.numeric(dataread2[,12])
	PR41 = as.numeric(dataread4[,11])
	PR42 = as.numeric(dataread4[,12])
	GHI=Tamb=NA
	if((!is.na(filepathm5))&&file.exists(filepathm5))
	{
		dataread5 = read.table(filepathm5,header = T,sep = "\t",stringsAsFactors=F) #its correct dont change
		GHI = as.numeric(dataread5[,2])
		Tamb = as.numeric(dataread5[,3])
	}
  GA1 = as.numeric(dataread2[,15])	
  PA1 = as.numeric(dataread2[,16])	
  GA2 = as.numeric(dataread4[,15])	
  PA2 = as.numeric(dataread4[,16])	
  GA = (GA1+GA2)/2	
  PA = (PA1+PA2)/2
	df = data.frame(Date = substr(as.character(dataread1[1,1]),1,10),
	DA= da,
	Downtime = dt,
	EacLoad1 = EacLoad1,
	EacLoad2 = EacLoad2,
	EacSol1Meth1=EacSol1Meth1,
	EacSol1Meth2=EacSol1Meth2,
	RatioSol1=RatioSol1,
	LastTimeSol1 = LastTimeSol1,
	LastReadSol1 = LastReadSol1,
	EacSol2Meth1=EacSol2Meth1,
	EacSol2Meth2=EacSol2Meth2,
	RatioSol2=RatioSol2,
	LastTimeSol2 = LastTimeSol2,
	LastReadSol2 = LastReadSol2,
	GsiTot = GsiTot,
	PRM1 = PR11,
	PRM2Meth1 = PR21,
	PRM2Meth2 = PR22,
	PRM3 = PR31,
	PRM4Meth1 = PR41,
	PRM4Meth2 = PR42,
	ArtificialM1 = artificial1,
	ArtificialM2 = artificial2,
	GHI=GHI,Tamb=Tamb,
 	GA1=GA1,	
  PA1=PA1,	
  GA2=GA2,	
  PA2=PA2,	
  GA=GA,	
  PA=PA,
	stringsAsFactors=F)

  {
    if(file.exists(writefilepath))
    {
		
	data = read.table(writefilepath,header=T,sep="\t",stringsAsFactors=F)
	dates = as.character(data[,1])
	idxmtch = match(substr(as.character(dataread1[1,1]),1,10),dates)
	if(is.finite(idxmtch))
	{
		data = data[-idxmtch,]
		write.table(data,file = writefilepath,row.names = F,col.names = T,sep = "\t",append = F)
	}
      write.table(df,file = writefilepath,row.names = F,col.names = F,sep = "\t",append = T)
    }
    else
    {
      write.table(df,file = writefilepath,row.names = F,col.names = T,sep = "\t",append = F)
    }
  }
}

