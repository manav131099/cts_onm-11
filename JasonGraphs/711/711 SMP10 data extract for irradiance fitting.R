rm(list=ls(all =TRUE))

###     IN-711 DATA EXTRACTION       ###
pathRead <- "/home/admin/Dropbox/Cleantechsolar/1min/[711]"
pathWrite <- "/home/admin/Jason/cec intern/results/711/[711]_da_summary.txt"
pathWrite2 <- "/home/admin/Jason/cec intern/results/711/[711]_da_summary.CSV"
setwd(pathRead)
filelist <- dir(pattern = ".txt", recursive= TRUE)

nameofStation <- "711"

col0 <- c()
col1 <- c()
col2 <- c()
col3 <- c()
col4 <- c()
col5 <- c()
col6 <- c()
col7 <- c()
col8 <- c()
col0[10^6] = col1[10^6] = col2[10^6] = col3[10^6] = col4[10^6] = col5[10^6] = col6[10^6] = col7[10^6]= col8[10^6]=0
index <- 1

for (i in filelist){
  data <- NULL
  temp <- read.table(i,header = T, sep = "\t")
  date <- substr(paste(temp[1,1]),1,10)
  col0[index] <- nameofStation
  col1[index] <- date
  #data availability
  pts <- length(temp[,2])
  col3[index] <- pts
  gsi <- as.numeric(temp[,5])     #5 is for usual wallgraph. 4 for paper. 6. IS SMP10
  col4[index] <- sum(gsi)/60000
  print(paste(i, "done"))
  index <- index + 1
}

col0 <- col0[1:(index-1)]
col1 <- col1[1:(index-1)]
col2 <- col2[1:(index-1)]
col3 <- col3[1:(index-1)]
col4 <- col4[1:(index-1)]
col2 <- col3/max(col3)*100
col2[is.na(col2)] <- NA              #states T/F
col3[is.na(col3)] <- NA
col4[is.na(col4)] <- NA

print("Starting to save..")

result <- cbind(col1,col2,col4)
colnames(result) <- c("Date","DA","SMP10")

for(x in c(2,3)){
  result[,x] <- round(as.numeric(result[,x]),1)
  #print(result[,x])
}

rownames(result) <- NULL
result <- data.frame(result)    
dataWrite <- result
write.table(result,na = "",pathWrite,row.names = FALSE,sep ="\t")
write.csv(result,pathWrite2, na= "", row.names = FALSE)


