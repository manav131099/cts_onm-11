rm(list = ls())
handle1=file("/home/admin/Logs/Logs-PH_Market_Digest.txt",open='wt',encoding='UTF-8')
handle="/home/admin/Dropbox/Second Gen/"
sitesumm="/home/admin/CODE/AllSites/PH/PH_Allsite.csv"

sink(handle1,type='message',append = T)
require('mailR')
source('/home/admin/CODE/common/math.R')
mailbody=function(sa,handle,sitesumm)
{
  e=c()
  site=c()
  sitename=c()
  syssize=c()
  s=as.Date(sa)
  message(paste("Enter for Digest for ",as.character(s)))
  atn=c()
  dz<-read.csv(sitesumm,header = T,encoding = "UTF-8")
  for (qw in 1:(nrow(dz))) 
  {
    site=c(site,as.character(dz[qw,1]))
    sitename=c(sitename,as.character(dz[qw,2]))
    syssize=c(syssize,as.numeric(dz[qw,3]))
  }
  
  body=""
  body = paste(body,"Market: Philippines\n\n",sep="")
  body = paste(body,"Number of Systems: ",nrow(dz),sep="")
  
  
  for (kj in 1:length(site)) 
  {
    if(as.character(site[kj])=="PH-006S")
    {
      path=handle
      body = paste(body,"\n\n---------------------------------------------------------------------------------",sep="")
      body = paste(body,"\n",site[kj]," ", sitename[kj]," ", syssize[kj]," ", "kWp",sep="")
      body = paste(body,"\n---------------------------------------------------------------------------------",sep="")
      path=paste(path,"[",site[kj],"]","/",sep="")
      path=paste(path,substr(s,1,4),"/",substr(s,1,7),"/",sep="")
      names=list.files(path)
      if(length(names)>0)
      {
        pat=grepl(s,names)
        n1=NA
        for(i in 1:length(pat))
        {
          if(pat[i]==TRUE)
          {
            n1=as.character(names[i])
          }
        }
        path=paste(path,n1,sep="")
        if(file.exists(path))
        {
          df<-read.table(path,header = TRUE,sep = "\t")
          body = paste(body,"\n\nEAC2 system generated power [kWh]: ",as.character(df[1,19]),sep="")
          body = paste(body,"\n\nDaily specific yield [kWh/kWp]: ",as.character(df[1,23]),sep="")
          body = paste(body,"\n\nPerfomance Ratio Method 2 (%): ",as.character(df[1,21]),sep="")
          body = paste(body,"\n\nDaily Irradiation [kWh/m2]: ",as.character(df[1,3]),sep="")
          e=c(e,as.numeric(df[1,13]))
          atnChk = as.numeric(df[1,15])
          if(!is.na(atnChk))
          {
            if(as.numeric(atnChk)<60)
              atn=c(atn,site[kj])
          }
          
        }
        else
          body = paste(body,"\n\nData Not Available",sep="")
      }
      else
        body = paste(body,"\n\nData Not Available for a Long Time",sep="")
    } 
    else
     {
    path=handle
    body = paste(body,"\n\n---------------------------------------------------------------------------------",sep="")
    body = paste(body,"\n",site[kj]," ", sitename[kj]," ", syssize[kj]," ", "kWp",sep="")
    body = paste(body,"\n---------------------------------------------------------------------------------",sep="")
    path=paste(path,"[",site[kj],"]","/",sep="")
    path=paste(path,substr(s,1,4),"/",substr(s,1,7),"/",sep="")
    names=list.files(path)
    if(length(names)>0)
    {
      pat=grepl(s,names)
      n1=NA
      for(i in 1:length(pat))
      {
        if(pat[i]==TRUE)
        {
          n1=as.character(names[i])
        }
      }
      path=paste(path,n1,sep="")
      if(file.exists(path))
      {
        df<-read.table(path,header = TRUE,sep = "\t")
        body = paste(body,"\n\nEAC2 system generated power [kWh]: ",as.character(df[1,3]),sep="")
        body = paste(body,"\n\nDaily specific yield [kWh/kWp]: ",as.character(df[1,13]),sep="")
        body = paste(body,"\n\nPerfomance Ratio Method 2 (%): ",as.character(df[1,15]),sep="")
        body = paste(body,"\n\nDaily Irradiation [kWh/m2]: ",as.character(df[1,9]),sep="")
        e=c(e,as.numeric(df[1,13]))
        atnChk = as.numeric(df[1,15])
        if(!is.na(atnChk))
        {
          if(as.numeric(atnChk)<60)
            atn=c(atn,site[kj])
        }
        
      }
      else
        body = paste(body,"\n\nData Not Available",sep="")
    }
    else
      body = paste(body,"\n\nData Not Available for a Long Time",sep="")
   }
  }
  body = paste(body,"\n\n---------------------------------------------------------------------------------",sep="")
  body = paste(body,"\nMeteorological Station Name: PH-719 Cebu",sep="")
  body = paste(body,"\n---------------------------------------------------------------------------------",sep="")
  path=paste(handle,"/[PH-719S]","/",sep="")
  path=paste(path,substr(s,1,4),"/",substr(s,1,7),"/",sep="")
  names=list.files(path)
  if(length(names)>0)
  {
    pat=grepl(s,names)
    n1=NA
    for(i in 1:length(pat))
    {
      if(pat[i]==TRUE)
      {
        n1=as.character(names[i])
      }
    }
    path=paste(path,n1,sep="")
    if(file.exists(path))
    {
      dataread<-read.table(path,header = TRUE,sep = "\t")
      body = paste(body,"\n\nDaily Global Horizontal Irradiation, Pyranometer [kWh/m2]:",as.character(dataread[1,3]))
      body = paste(body,"\n\nDaily Global Horizontal Irradiation, Silicon Sensor [kWh/m2]:",as.character(dataread[1,4]))
      body = paste(body,"\n\nDaily Tilted Global Irradiation, Silicon Sensor, dirty [kWh/m2]:",as.character(dataread[1,5]))
      body = paste(body,"\n\nMean Ambient Temp [C]:",as.character(dataread[1,6]))
      body = paste(body,"\n\nMean Ambient Temp Solar Hours [C]:",as.character(dataread[1,7]))
      body = paste(body,"\n\nMean Humidity [%]:",as.character(dataread[1,12]))
      body = paste(body,"\n\nMean Humidity Solar Hours [%]:",as.character(dataread[1,13]))
      body = paste(body,"\n\nPyr/Si Ratio:",as.character(dataread[1,22]))
      body = paste(body,"\n\nGmod/Gsi Ratio:",as.character(dataread[1,23]))
    }
    else
      body = paste(body,"\n\nData Not Available",sep="")
  }
  else
      body = paste(body,"\n\nData Not Available for a long time",sep="")
  
  
  
  
  
  body = paste(body,"\n\n---------------------------------------------------------------------------------",sep="")
  body = paste(body,"\n---------------------------------------------------------------------------------",sep="")
  e=e[!is.na(e)]
  body = paste(body,"\n\nStdDev [Yield]: ",formatC(sdp(e),format="f",digits = 2,big.mark = ",",big.interval = 3L),sep="")
  body = paste(body,"\n\nCoV [Yield]: ",formatC((sdp(e)*100)/mean(e),format="f",digits = 2,big.mark = ",",big.interval = 3L),"[%]",sep="")
  body = paste(body,"\n\nInstalled Capacity [kWp]: ",formatC(sum(syssize),format="f",digits = 2,big.mark = ",",big.interval = 3L),sep="")
  body = paste(body,"\n\nAverage System Size [kWp]: ",formatC(mean(syssize),format="f",digits = 2,big.mark = ",",big.interval = 3L),sep="")
  sit=atn
  b=list(body,atn,sit)
  message("Mail Digest Done")
  return(b)
}
# Mail section
sender <- "operations@cleantechsolar.com" 
uname <- 'shravan.karthik@cleantechsolar.com'
pwd = 'CTS&*(789'
recipients <- c("rupesh.baker@cleantechsolar.com")
sendToken = 0
forceSendToken = 0
while (1) 
{ 
  datePrev=as.character(as.Date(substr(format(Sys.time(),tz="Singapore"),1,10))-1)
  hourNow = format(Sys.time(),tz = "Singapore")
  hrNow = as.numeric(substr(hourNow,12,13))
  if(hrNow == 1 || hrNow == 2)
    sendToken = 1
  if(((hrNow == 7 || hrNow == 8) && sendToken) || forceSendToken)
  {
    message(format(Sys.time(),tz=Sys.timezone()))
    message("Mail Section")
    body=mailbody(as.Date(datePrev),handle,sitesumm)
    if(length(body[[2]])>0)
      sub=paste("[",as.character(datePrev),"]"," ","<WARNING-PR for ",body[[3]]," > PH All Site Summary", sep = "")
    else
      sub=paste("[",as.character(datePrev),"]"," ","PH All Site Summary", sep = "")
    send.mail(from = sender,
              to = recipients,
              subject = sub,
              body = as.character(body[[1]]),
              smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
              authenticate = TRUE,
              send = TRUE,
              debug = F)
    message("Mail Sent")
    lastmail=datePrev
    message(paste("Last mail sent for: ",lastmail))
    message("\n----------------------------------------------------------------")
    sendToken = 0
    forceSendToken = 0
  }
  message("Going to sleep...")
  Sys.sleep(3600)
}
sink() 
