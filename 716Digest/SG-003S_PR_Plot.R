rm(list=ls(all =TRUE))
library(ggplot2)
library(zoo)
library(lubridate)

args <- commandArgs(TRUE)
date=args[1]

#Using 3rd gen data; not available in sharepoint
pathRead <- "/home/admin/Dropbox/Second Gen/[SG-003S]"



#extracting data from 3rd Gen
setwd(pathRead)
filelist <- dir(pattern = "([0-9]{4}).txt", recursive= TRUE)
dff = NULL
for(z in filelist[1:length(filelist)])
{
  if(grepl("16",z))
		next
  temp <- read.table(z, header = T, sep = "\t")  
  df <- temp[,c(1,3,26,27,28)]
  dff = rbind(dff, df)
}
dff = as.data.frame(dff)
dff['PR'] = (dff[3]*839.52 + dff[4]*782.28 + dff[5]*165.36)/(839.52+782.28+165.36)

dff_M1 <- dff[, c(1,2,3)]
names(dff_M1)[3] <- "PR"
dff_M2 <- dff[, c(1,2,4)]
names(dff_M2)[3] <- "PR"
dff_M3 <- dff[, c(1,2,5)]
names(dff_M3)[3] <- "PR"
dff <- dff[, c(1,2,6)]
df_list= list(dff,dff_M1,dff_M2,dff_M3)

cnt=0
for(m in df_list){
  cnt=cnt+1
for(x in 1 :nrow(m))
{
	if(is.finite(m[x,3]) && (as.numeric(m[x,3]) < 10))
		m[x,3] = NA
}

m = m[complete.cases(as.numeric(m[,3])),]
m = m[(m[,3] <= 95),]
m = m[(m[,3] >= 0),]
m = m[(as.Date(m$Date)<= date),]
m = unique(m)
row.names(m) <- NULL

#moving average 30d
zoo.PR <- zoo(m[,3], as.Date(m$Date))
ma1 <- rollapplyr(zoo.PR, list(-(29:1)), mean, fill = NA, na.rm = T)
m$ambPR.av = coredata(ma1)
row.names(m) <- NULL

firstdate <- as.Date(m[1,1])
lastdate <- as.Date(m[length(m[,1]),1])

PRUse = as.numeric(m$PR)
PRUse = PRUse[complete.cases(PRUse)]
last30 = round(mean(tail(PRUse,30)),1)
last60 = round(mean(tail(PRUse,60)),1)
last90 = round(mean(tail(PRUse,90)),1)
last365 = round(mean(tail(PRUse,365)),1)
lastlt = round(mean(PRUse),1)
rightIdx = nrow(m) * 0.7
x1idx = round(nrow(m) * 0.20, 0)
x2idx = round(nrow(m) * 0.3, 0)
x3idx = round(nrow(m) * 0.45, 0)

#data sorting for visualisation
m$colour[m[,2] < 2] <- '< 2'
m$colour[m[,2] >= 2 & m[,2] <= 4] <- '2 ~ 4'
m$colour[m[,2] >= 4 & m[,2] <= 6] <- '4 ~ 6'
m$colour[m[,2] > 6] <- '> 6'
m$colour = factor(m$colour, levels = c("< 2", "2 ~ 4", "4 ~ 6", "> 6"), labels =c('< 2', '2 ~ 4', '4 ~ 6', '> 6')) #to fix legend arrangement
titlesettings <- theme(plot.title = element_text(face = "bold", size = 12, lineheight = 0.7, hjust = 0.5, margin = margin(0,0,7,0)), plot.subtitle = element_text(face = "bold", size = 12, lineheight = 0.9, hjust = 0.5))

p <- ggplot() + theme_bw()
p <- p + geom_point(data=m, aes(x=as.Date(Date), y = PR, shape = as.factor(18), colour = colour), size = 2) + guides(shape = FALSE)
p <- p + ylab("Performance Ratio [%]") + xlab("") + coord_cartesian(ylim = c(0,100))
p <- p + scale_x_date(expand = c(0, 0),date_breaks = "3 months", date_labels = "%b/%y", limits = c(firstdate, NA)) + scale_y_continuous(breaks=seq(0, 100, 10))
p <- p + theme(axis.title.x = element_text(size=13), axis.title.y = element_text(size=13), axis.text = element_text(size=13), panel.grid.minor = element_blank(),                 panel.border = element_blank(), axis.line = element_line(colour = "black"), legend.justification = c(0, 0.97), legend.position = c(0.1, 0.97))                     #LEGNED AT RHS
if(cnt==1){
  p <- p + titlesettings + ggtitle(paste0('Performance Ratio Evolution - SG-003-','Full Site'), subtitle = paste0('From ', firstdate, ' to ', lastdate))
}else{
p <- p + titlesettings + ggtitle(paste0('Performance Ratio Evolution - SG-003-M',cnt-1), subtitle = paste0('From ', firstdate, ' to ', lastdate))
}
p <- p + theme(legend.text=element_text(size=9), legend.title=element_text(size=11))
p <- p + scale_colour_manual('Daily Irradiation [kWh/m2]', values = c("blue", "deepskyblue1", "orange","darkorange3"), labels = c('< 2','2 ~ 4','4 ~ 6','> 6'),                                 guide = guide_legend(title.position = "left", ncol = 4, nrow=1)) 
p <- p + scale_shape_manual(values = 18)
p <- p + theme(legend.box = "horizontal", legend.direction="horizontal") #legend.position = "bottom"
p <- p + guides(colour = guide_legend(override.aes = list(shape = 18)))
p <- p + geom_line(data=m, aes(x=as.Date(Date), y=ambPR.av), color="red", size=1.5) 

rate=0.008
prbudget=77.1
coddate=as.Date('2017-03-01')

#Creating steps using budget PR
prthresh = c(prbudget,prbudget)
dates = c(firstdate,coddate)
temp=prbudget
while(coddate<as.Date(date) && (as.Date(date)-coddate)>365){
     coddate=coddate %m+%months(12)
     temp=temp-(temp*rate)
     prthresh=c(prthresh,temp)
     dates=c(dates,coddate)
}
prthresh=c(prthresh,temp)
dates=c(dates,lastdate)
p <- p +geom_step(mapping=aes(x=dates,y=prthresh),size=1,color='darkgreen')
m$Date=as.Date(m$Date, format = "%Y-%m-%d")
total=0
text="Target Budget Yield Performance Ratio ["
for(i in 2:(length(dates)-1)){#n-1 because we are taking number of years between. 
    if(i==(length(prthresh)-1)){
    text=paste(text,i-1,'Y-',format(round(prthresh[i],1),nsmall=1), "%]", sep="")
    }else{
    text=paste(text,i-1,'Y-',format(round(prthresh[i],1),nsmall=1), "%,", sep="")
    }
}

for(i in 1:(length(dates)-1)){
    if(i==(length(dates)-1)){
      total=total+sum(m[m$Date >= dates[i] & m$Date <=dates[i+1],'PR']>prthresh[i])
    }else{
      total=total+sum(m[m$Date >= dates[i] & m$Date < dates[i+1],'PR']>prthresh[i])
    }
} 

p <- p + annotate("segment", x = as.Date(m[nrow(m)* 0.25,1]), xend = as.Date(m[nrow(m)* 0.28,1]), y=50, yend=50, colour="darkgreen", size=1.5)
p <- p + annotate("segment", x = as.Date(m[nrow(m)* 0.25,1]), xend = as.Date(m[nrow(m)* 0.28,1]), y=45, yend=45, colour="red", size=1.5)
p <- p + annotate('text', label = text, y = 50, x = as.Date(m[x2idx,1]), colour = "darkgreen",fontface =2,hjust = 0)
p <- p + annotate('text', label = paste("Points above Target Budget PR = ",total,'/',nrow(m),' = ',round((total/nrow(m))*100,1),'%',sep=''), y = 40, x = as.Date(m[x2idx,1]), colour = "black",fontface =2,hjust = 0)
p <- p + annotate('text', label = "30-d moving average of PR", y = 45, x = as.Date(m[x2idx,1]), colour = "red",fontface =2,hjust = 0)
p <- p + annotate('text', label = paste("Average PR last 30-d:", last30, "%"), y=25, x=as.Date(m[rightIdx,1]), colour="black", hjust = 0)
p <- p + annotate('text', label = paste("Average PR last 60-d:", last60, "%"), y=20, x=as.Date(m[rightIdx,1]), colour="black", hjust = 0)
p <- p + annotate('text', label = paste("Average PR last 90-d:", last90, "%"), y=15, x=as.Date(m[rightIdx,1]), colour="black", hjust = 0)
p <- p + annotate('text', label = paste("Average PR last 365-d:", last365,"%"), y=10, x=as.Date(m[rightIdx,1]), colour="black", hjust = 0)
p <- p + annotate('text', label = paste("Average PR Lifetime:", lastlt,"%"), y=5, x=as.Date(m[rightIdx,1]), colour="black", hjust = 0,fontface =2)
p <- p + annotate("segment", x = as.Date(m[nrow(m),1]), xend = as.Date(m[nrow(m),1]), y=0, yend=100, colour="deeppink4", size=1.5, alpha=0.5)

if(cnt==1){
pathwritetxt <- paste("/home/admin/Graphs/Graph_Extract/SG-003/[SG-003] Graph ", date, " - PR Evolution-","Full Site",".txt", sep="")
graph <- paste("/home/admin/Graphs/Graph_Output/SG-003/[SG-003] Graph ", date, " - PR Evolution-","Full Site",".pdf", sep="")
}else{
pathwritetxt <- paste("/home/admin/Graphs/Graph_Extract/SG-003/[SG-003] Graph ", date, " - PR Evolution-M",cnt-1,".txt", sep="")
graph <- paste("/home/admin/Graphs/Graph_Output/SG-003/[SG-003] Graph ", date, " - PR Evolution-M",cnt-1,".pdf", sep="")
}

print(graph)
ggsave(paste0(graph), p, width = 11, height=8)
write.table(m, na = "", pathwritetxt, row.names = FALSE, sep ="\t")
}
